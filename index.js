#!/usr/bin/env node
const io = require('socket.io-client');
const { Chess } = require('chess.js');

var socket = io.connect('https://tcec-chess.com/');

socket.on('pgn', data => {
  console.log('\033[2J');
  console.log(data.Headers);
  
  const chess = new Chess(data.Moves[1].fen);
  console.log(chess.ascii());	
});


